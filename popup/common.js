/**
 * Listen for clicks on the buttons, and send the appropriate message to
 * the content script in the page.
 */
function listenForClicks() {
    function fillFromJson() {
        let jsonTemplate = document.querySelector("#actions");

        const storageItem = browser.storage.sync.get('json');

        let index = 0;

        storageItem.then((res) => {
            if (undefined != res.json) {

                let actions = JSON.parse(res.json);
                for (let x in actions) {
                    let row = actions[x];

                    for (let y in row) {
                        let item = row[y];

                        for (let a in item) {

                            let itemItem = item[a];


                            let tr = document.createElement('tr');
                            let th = document.createElement('th');
                            let td = document.createElement('td');

                            ++index;
                            th.innerHTML = index;


                            for (let v in itemItem) {

                                let title = itemItem[v].title;
                                let command = itemItem[v].action;

                                let button = document.createElement("button");
                                button.setAttribute('data-command', command);
                                button.setAttribute('class', 'btn btn-outline-dark action m-1 ');
                                button.setAttribute('type', 'button');
                                button.innerHTML = title;

                                td.appendChild(button);
                            }
                            tr.appendChild(th);
                            tr.appendChild(td);


                            jsonTemplate.appendChild(tr);

                        }


                    }


                }

            }

        });

    }


    fillFromJson();

    document.addEventListener("click", (e) => {


        function sendAction(tabs) {
            browser.tabs.sendMessage(tabs[0].id, {
                command: e.target.getAttribute('data-command'),
            });
        }


        function reset(tabs) {
            browser.tabs.sendMessage(tabs[0].id, {
                command: "reset",
            });
        }

        function commit(tabs) {
            browser.tabs.sendMessage(tabs[0].id, {
                command: "commit",
            });
        }

        function notes(tabs) {
            browser.tabs.sendMessage(tabs[0].id, {
                command: "notes",
            });
        }

        /**
         * Just log the error to the console.
         */
        function reportError(error) {
            console.error(`Could not gitlab assistant: ${error}`);
        }

        /**
         * Get the active tab,
         * then call "sendAction()" or "reset()" as appropriate.
         */

        if (e.target.classList.contains("action")) {
            browser.tabs.query({active: true, currentWindow: true})
                .then(sendAction)
                .catch(reportError);
        } else if (e.target.classList.contains("reset")) {
            browser.tabs.query({active: true, currentWindow: true})
                .then(reset)
                .catch(reportError);
        } else if (e.target.classList.contains("commit")) {
            browser.tabs.query({active: true, currentWindow: true})
                .then(commit)
                .catch(reportError);
        } else if (e.target.classList.contains("notes")) {
            browser.tabs.query({active: true, currentWindow: true})
                .then(notes)
                .catch(reportError);
        }
    });
}

/**
 * There was an error executing the script.
 * Display the popup's error message, and hide the normal UI.
 */
function reportExecuteScriptError(error) {
    document.querySelector("#popup-content").classList.add("hidden");
    document.querySelector("#error-content").classList.remove("hidden");
    console.error(`Failed to execute Gitlab assistant content script: ${error.message}`);
}

/**
 * When the popup loads, inject a content script into the active tab,
 * and add a click handler.
 * If we couldn't inject the script, handle the error.
 */
browser.tabs.executeScript({file: "/content_scripts/actions.js"})
    .then(listenForClicks)
    .catch(reportExecuteScriptError);
